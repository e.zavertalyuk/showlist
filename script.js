"use strict"

// Опишіть, як можна створити новий HTML тег на сторінці.
// let el = document.createElement('tag');
// document.body.append(el);


// Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.
// Где?

// `beforebegin' - вставить html перед elem,
// `afterbegin' - вставить html в начало elem,
// `beforeend' - вставить html в конец elem,
// `afterend' - вставитиь html после elem.

// Як можна видалити елемент зі сторінки?
// Node.remove();

function showList(list) {

   function showUl(show, Ul) {
       let ul = document.createElement('ul');
       let li;

       show.appendChild(ul);

       Ul.forEach(function (item) {
           if (Array.isArray(item)) {
               showUl(li, item);
               return;
           }

           li = document.createElement('li');
           li.appendChild(document.createTextNode(item));
           ul.appendChild(li);
       });
   }

   let div = document.getElementById('thisList');

   showUl(div, list);
};
showList(['hello', 'world', ["Hello", "Kiev"],'Kiev', 'Kharkiv', 'Odessa', 'Lviv']);



